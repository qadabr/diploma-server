import csv
import os
from datetime import *

KEY_LOGGER_RESULTS = "results/%s/key_logger/%s"


def show_files(id_hash):
    return os.listdir('results/%s/key_logger/' % id_hash)


def process_data(csv_name):
    columns = ['timestamp', 'shift', 'value', 'down']

    X = []
    processed_keys = []
    prc_file = open(csv_name + '.prc', 'w')

    with open(csv_name, 'r', newline='') as csv_file:
        reader = csv.DictReader(csv_file, fieldnames=columns)

        last_down = 0
        for row in reader:
            down = int(row['down'])

            if down == 1:
                last_down = int(row['timestamp'])
                processed_keys.append(row)
                continue

            if down == 0:
                for key in processed_keys:
                    if int(row['value']) == int(key['value']):
                        processed_keys.remove(key)
                        row_ts = int(row['timestamp'])
                        key_ts = int(key['timestamp'])
                        overlapping = len(processed_keys)
                        if last_down != 0:
                            prc_file.write('%d,%d,%d\n' % (row_ts - key_ts, row_ts - last_down, overlapping))
                            X.append([row_ts - key_ts, row_ts - last_down, overlapping])
                        else:
                            prc_file.write('%d,%d,%d\n' % (row_ts - key_ts, row_ts - key_ts, overlapping))
                            X.append([row_ts - key_ts, row_ts - key_ts, overlapping])

    prc_file.close()


def save_csv(id_hash, data):
    csv_name = KEY_LOGGER_RESULTS % \
               (id_hash, datetime.now().strftime('%d.%m.%Y_%H:%M:%S') + '.csv')

    os.makedirs(os.path.dirname(csv_name), exist_ok=True)

    with open(csv_name, 'w') as f:
        f.write(data)

    process_data(csv_name)
