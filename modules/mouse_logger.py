import csv
import os
from datetime import *

MOUSE_LOGGER_RESULTS = "results/%s/mouse_logger/%s"


def save_csv(id_hash, data):
    csv_name = MOUSE_LOGGER_RESULTS % \
               (id_hash, datetime.now().strftime('%d.%m.%Y_%H:%M:%S') + '.csv')

    os.makedirs(os.path.dirname(csv_name), exist_ok=True)

    with open(csv_name, 'w') as f:
        f.write(data)