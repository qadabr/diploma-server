from peewee import *

db = SqliteDatabase('agents.db')


class Agent(Model):
    id = PrimaryKeyField()
    id_hash = CharField()
    node_name = CharField()
    ip_address = CharField()
    upd_time = DateTimeField()

    class Meta:
        database = db
        table_name = 'Agents'
